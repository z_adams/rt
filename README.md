## Definitions

Coordinate systems for the world space are complicated because compass azimuth
is measured in angles increasing in the negative direction, whereas cylindrical
and spherical azimuths increase in the positive direction (from X towards Y).

If the zero azimuth from spherical coordinates is chosen as North, the
cartesian coordinates (X, Y, Z) then map to the cardinal directions (N, W, U)
(U being the upwards direction). This is a little counterintuitive because one
typically thinks of X and East both as the lateral direction, so the azimuthal
angle is rotated by +90 degrees and flipped such that an azimuth of 0 points in
positive Y and increases towards X, making the mapping (X, Y, Z) -> (E, N, U),
yielding the standard left-handed compass azimuth convention and the more
intuitive orientation when thinking about the layout of the world map, as 
viewed from the top down like a standard cartesian plane.

The effect this has is allowing the interface to be handled from the typical
pilot perspective, where a plane pointing in the direction (0, 1, 0) is flying
due North, and to specify beam steering relative to the nose accordint to the
compass.

Raytracing is performed from the vehicle's coordinate space relative to the
boreline, and only converted to world space to cast the individual rays. The
display mapping functions also take data from the aircraft's point of view,
which is then later internally rotated to align to the display output.

## System Structure

---

The structure of the raytracer tends to follow a dependency injection layout
and is as follows:

The raytracer, either optical or radio, is a class that contains the means to
perform raytracing using the information provided in its data member object.
The data object contains implementation relevant details (e.g. radar vs 
optical), and a scene manager object which keeps track of scene geometry, 
object visibility, and the algorithm which performs the ray-triangle 
intersection probe into the scene.

The ObjectManager holds a fixed array of scene meshes, as well as a fixed array
of scene objects. The objects reference the scene meshes and store the position
and rotation of that particular instance. The geometry arrays are loaded in
from disk by the ObjParser class in the setup step into an STL Vector, which is
then deep copied into the fixed array held by the ObjectManager.

The ObjectManager is a black box to the raytracer, simply taking a raycast and
returning the intersection data. Therefore it is responsible for any
accleration structures involved in the data search. A sparse voxel octree is
implemented, the volumes of which contain pointers to scene geometry for rapid
searching. This capability is possible both for whole-object bounding and for
sub-object bounding of individual triangles such as when raytracing a large
terrain mesh.

The raytracers contain functions which perform the main casting of rays
into the scene. The scene data is sampled by the object manager and returns the
information about the impact, such as which mesh and triangle were hit, and the
texture coordinates of the hit. The raytracer uses this information to perform
the necessary sampling and shading, and then writes the resulting data to its
output image. The radar raytracer uses the RadarScope class to package details
regarding mapping the resulting data into a realistic display output.
