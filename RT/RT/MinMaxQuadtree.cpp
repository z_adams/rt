#include "MinMaxQuadtree.h"

bool Boundary::contains(Vec3D_t point) const
{
    if (point.x < (position.x + (xyWidth / 2)) &&
        point.x >(position.x - (xyWidth / 2)) &&
        point.y < (position.y + (xyWidth / 2)) &&
        point.y >(position.y - (xyWidth / 2)) &&
        point.z < zMax &&
        point.z > zMin)
    {
        return true;
    }
    return false;
}

bool Boundary::intersectedBy(const Ray ray, float t0, float t1) const
{
    if (contains(ray.origin))
        return true;
    float tmin, tmax, tymin, tymax, tzmin, tzmax;
    if (ray.direction.x >= 0)
    {
        tmin = (xMinBound() - ray.origin.x) / ray.direction.x;
        tmax = (xMaxBound() - ray.origin.x) / ray.direction.x;
    } else
    {
        tmin = (xMaxBound() - ray.origin.x) / ray.direction.x;
        tmax = (xMinBound() - ray.origin.x) / ray.direction.x;
    }
    if (ray.direction.y >= 0)
    {
        tymin = (yMinBound() - ray.origin.y) / ray.direction.y;
        tymax = (yMaxBound() - ray.origin.y) / ray.direction.y;
    } else
    {
        tymin = (yMaxBound() - ray.origin.y) / ray.direction.y;
        tymax = (yMinBound() - ray.origin.y) / ray.direction.y;
    }
    if ((tmin > tymax) || (tymin > tmax))
        return false;
    if (tymin > tmin)
        tmin = tymin;
    if (tymax < tmax)
        tmax = tymax;
    if (ray.direction.z >= 0)
    {
        tzmin = (zMinBound() - ray.origin.z) / ray.direction.z;
        tzmax = (zMaxBound() - ray.origin.z) / ray.direction.z;
    } else
    {
        tzmin = (zMaxBound() - ray.origin.z) / ray.direction.z;
        tzmax = (zMinBound() - ray.origin.z) / ray.direction.z;
    }
    if ((tmin > tzmax) || (tzmin > tmax))
        return false;
    if (tzmin > tmin)
        tmin = tzmin;
    if (tzmax < tmax)
        tmax = tzmax;
    return ((tmin < t1) && (tmax > t0));
}