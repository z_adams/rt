#include "Mesh.h"

SceneObject::SceneObject()
{
    SceneObject(Vec3D_t(0.0f, 0.0f, 0.0f), 
        Matrix4x4::computeRotationMatrix(0.0f, 0.0f, 0.0f));
}

Mesh::Mesh(std::shared_ptr<Face[]> faceArray, std::shared_ptr<Vec3D_t[]> vertexArray, std::shared_ptr<Vec3D_t[]> normArray, 
    std::shared_ptr<Vec2D_t[]> uvArray, int nVerts, int nFaces, int nNormals, int nUVs, 
    std::string textureFilepath)
{
    faces = faceArray;
    vertices = vertexArray;
    normals = normArray;
    uvs = uvArray;
    texture = std::shared_ptr<BmpImage>(new BmpImage(textureFilepath));
    this->nVerts = nVerts;
    this->nFaces = nFaces;
    this->nNormals = nNormals;
    this->nUVs = nUVs;
}

Mesh::Mesh(const Mesh &other) : Mesh()
{
    (*this) = other;
}

Mesh& Mesh::operator=(const Mesh & other)
{
    faces = other.faces;
    vertices = other.vertices;
    normals = other.normals;
    uvs = other.uvs;
    texture = other.texture;
    nVerts = other.nVerts;
    nFaces = other.nFaces;
    nNormals = other.nNormals;
    nUVs = other.nUVs;
    return *this;
}

/* Takes the coordinate of the intersection, the view direction, the face being
 * hit, the barycentric coordinates of the hit (uv), and returns the normal
 * vector at the surface, and the texture coordinates of the hit point
*/
SurfaceProperties Mesh::getSurfaceProperties(
    const Vec3D_t & hitpoint, const Vec3D_t & viewDirection, 
    const Face &face, const Vec2D_t & uv) const
{
    Vec3D_t hitNormal;
    Vec2D_t texCoords;
    //hitNormal = face.tri.normal().normalize();  //TODO: interpolated surface normals
    Vec3D_t vn0 = normals[face.normIndex0];
    Vec3D_t vn1 = normals[face.normIndex1];
    Vec3D_t vn2 = normals[face.normIndex2];
    hitNormal = (vn0 * (1 - uv.x - uv.y)) + (vn1 * uv.x) + (vn2 * uv.y);
    //hitNormal = hitNormal.normalize();
    
    if (nUVs > 0)
    {
        Vec2D_t vt0 = uvs[face.uvIndex0];
        Vec2D_t vt1 = uvs[face.uvIndex1];
        Vec2D_t vt2 = uvs[face.uvIndex2];
        texCoords = (vt0 * (1 - uv.x - uv.y)) + (vt1 * uv.x) + (vt2 * uv.y);
    }
    SurfaceProperties properties = { hitNormal, texCoords };
    return properties;
    /*float w = 0.0f;
    Triangle::Barycentric(hitpoint, vt0, vt1, vt2,
        textureCoordinates.x, textureCoordinates.y, w);*/
}

void Mesh::bounds(Vec3D_t *origin, float *spanX, float *spanY, float *spanZ)
{
    float minX = 1e10, minY = 1e10, minZ = 1e10;
    float maxX = -1e10, maxY = -1e10, maxZ = -1e10;
    for (int i = 0; i < nVerts; i++)
    {
        Vec3D_t vert = vertices[i];
        if (vert.x > maxX)
            maxX = vert.x;
        else if (vert.x < minX)
            minX = vert.x;
        if (vert.y > maxY)
            maxY = vert.y;
        else if (vert.y < minY)
            minY = vert.y;
        if (vert.z > maxZ)
            maxZ = vert.z;
        else if (vert.z < minZ)
            minZ = vert.z;
    }
    *spanX = maxX - minX;
    *spanY = maxY - minY;
    *spanZ = maxZ - minZ;
    origin->x = (maxX + minX) / 2.0f;
    origin->y = (maxY + minY) / 2.0f;
    origin->z = (maxZ + minZ) / 2.0f;
}

