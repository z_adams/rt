#define _USE_MATH_DEFINES
#include "RadarScope.h"
#include "Vec3.h"

//BUG: effects of COLLAPSE_MIN_RANGE don't work for rasterizer
AggregatePixel& RadarScope::indexPolarPixel(float azimuth, float range)
{
    int rangeBin;
    if (COLLAPSE_MIN_RANGE)
    {
        rangeBin = (int)floorf((range - minRange) / (rangeBinWidth)); 
    } else
    {
        rangeBin = (int)floorf(range / rangeBinWidth);
    }
    int azimuthBin = 
        (int)floorf( (M_PI_2 + rad(foV/2) - azimuth ) / azimuthBinWidth );
    return pPArray[azimuthBin*maxRangeSegments + rangeBin];
}

void RadarScope::screenToRasterSpace(Vec2D_t & vec)
{
    vec.x += output->xWidth() / 2;
}

float RadarScope::rad(float degrees)
{
    return degrees * (M_PI / 180.0f);
}

float RadarScope::computeCornerRange(float range)
{
    /* Because the maximum range is measured from the bottom to top of the
     * display, the top corners are actually further from the origin than the
     * "max range," and thus the array actually needs to be larger
    */
    float displayXRes = rasterizedOutput->xWidth();
    float displayYRes = rasterizedOutput->yHeight();
    float aspectRatio = displayXRes / displayYRes;

    float halfHorzRange = (aspectRatio / 2.0f) * range;
    // hypotenuse from bottom center to top corner
    return sqrt(range*range + halfHorzRange*halfHorzRange);
}

void RadarScope::computeCornerRange()
{
    /* Because the maximum range is measured from the bottom to top of the
     * display, the top corners are actually further from the origin than the
     * "max range," and thus the array actually needs to be larger
    */
    float displayXRes = rasterizedOutput->xWidth();
    float displayYRes = rasterizedOutput->yHeight();
    float aspectRatio = displayXRes / displayYRes;

    float halfHorzRange = (aspectRatio / 2.0f) * maxRange;
    // hypotenuse from bottom center to top corner
    this->cornerRange = sqrt(maxRange*maxRange + halfHorzRange * halfHorzRange);
}

RadarScope::RadarScope(
    std::shared_ptr<BitMap> rasterizedOutput, int azimuthRes,
    float azimuthalBeamWidth, int rangeSegments, float rangeLimit,
    std::shared_ptr<BitMap> output) :
    /*output("../../rawOutput.bmp", displayXRes, displayYRes, colorDepth),
    rasterizedOutput("../../scopeOutput.bmp", output),*/
    rasterizedOutput(rasterizedOutput), azimuthBinWidth(0), cornerRange(0),
    foV(0), maxRangeSegments(0), minRange(0), pulsesPerSweep(0), 
    debugImgAvailable(false), rangeBinWidth(0), output(output)
{
    this->azimuthRes = azimuthRes;
    this->azimuthalBeamWidth = azimuthalBeamWidth;
    this->rangeSegments = rangeSegments;
    this->rangeLimit = rangeLimit;
    this->maxRange = rangeLimit;
    setFoV(120);

    pulsesPerSweep = foV / azimuthalBeamWidth;
    this->azimuthBinWidth = rad(foV) / pulsesPerSweep;
    changeRangeLimits(0.0f, 80.0f);
    if (output != nullptr)
        debugImgAvailable = true;
}

RadarScope::~RadarScope()
{
    pPArray.reset();
}

/* Bound a region by two azimuths.
 * Compute the range and azimuth of each pixel along that azimuth.
 * or somehow find them with slope and incrementing x and y.
 * Calculate the intersection with the other azimuth and current row.
 * Iterate over the interval to color it in.
 * Repeat for each column of pixels
*/
// TODO: Figure out why certain bins at range are saturated
void RadarScope::mapImage()
{
    float tmpR, tmpA, lftAzmBound, rtAzmBound, rangeWidth, pxWidth;
    int rangeBin, azimuthBin, xhalfRes;
    uint8_t value;
    AggregatePixel AP;
    Pixel fragColor;

    xhalfRes = rasterizedOutput->xWidth() / 2;
    lftAzmBound = rad((foV / 2) + 90.0f);
    rtAzmBound = rad(90.0f - (foV / 2));
    pxWidth = rasterizedOutput->yHeight() / maxRange;

    for (int y = 0; y < rasterizedOutput->yHeight(); y++)
    {
        for (int x = -xhalfRes; x < xhalfRes; x++)
        {
            tmpA = atan2(y, x);
            if (tmpA > lftAzmBound || tmpA < rtAzmBound)
                continue;
            tmpR = sqrt(x * x + y * y) / pxWidth;

            AP = indexPolarPixel(tmpA, tmpR);
            fragColor = fragShader(AP);

            rasterizedOutput->setPixel(x + xhalfRes, y, fragColor);
        }
    }
}

void RadarScope::changeRangeLimits(float min, float max)
{
    this->minRange = min;
    this->maxRange = max;

    computeCornerRange();
    rangeBinWidth = (max - min) / rangeSegments;
    this->maxRangeSegments = static_cast<int>(ceilf(cornerRange / rangeBinWidth));

    // area = double integral across the region a_0 to a_1 and r_0 to r_1, 
    // given by (1/2)(r_1^2 - r_2^2)(a_1 + a_2) = rangeInterval*angularInterval
    // rangePerSeg uses maxRange because maxRange corresponds to the range
    // specified by the argument
    float area, rangePerSeg = maxRange / rangeSegments;
    float r1 = 0, r2 = rangePerSeg;
    pPArray.reset();
    pPArray = std::unique_ptr<AggregatePixel[]>(new AggregatePixel[pulsesPerSweep*maxRangeSegments]);
    for (int y = 0; y < maxRangeSegments; y++)
    {
        r1 += rangePerSeg;
        r2 += rangePerSeg;
        area = 0.5 * (r2*r2 - r1*r1) * azimuthalBeamWidth;
        for (int x = 0; x < pulsesPerSweep; x++)
        {
            pPArray[x * maxRangeSegments + y].area = area;
            pPArray[x * maxRangeSegments + y].value = 0;
            pPArray[x * maxRangeSegments + y].nValues = 0;
        }
    }
}

bool RadarScope::setFoV(float foV)
{
    if (foV > 180)
    {
        return false;
    }
    this->foV = foV;
    return true;
}

Pixel RadarScope::fragShader(AggregatePixel bin)
{
    uint8_t value;
    if (bin.nValues == 0)
        value = 0x0;
    else
        value = (int)(bin.value / bin.nValues)*2 + 0xF;
    //area = bin->area;
    //value = static_cast<BYTE>((pAP->power / area) * 0xFF);     // Average samples over area (fuzzy)
    //value = static_cast<BYTE>((pAP->power.g / pAP->nValues) * 0xFF);  // Average samples over number (blown out)
    return Pixel(0x0, value, 0x0);
}

/* Takes the azimuth and range of a return sample and maps it to the display
*/
void RadarScope::mapPixelSpherical(float theta, RadarReturn &retn)
{
    if (COLLAPSE_MIN_RANGE)
    {
        if (retn.range < minRange || retn.range > maxRange)
        {
            return;
        }
    } else if (retn.range < 0 || retn.range > cornerRange)
    {
        return;
    }

    theta += M_PI_2; // Rotate from aircraft azimuthal frame to display space

    indexPolarPixel(theta, retn.range).value += retn.power;
    indexPolarPixel(theta, retn.range).nValues++;
}

void RadarScope::mapPoint(float theta, RadarReturn & retn)
{
    if (COLLAPSE_MIN_RANGE) 
    {
        if (retn.range < minRange || retn.range > maxRange)
        { 
            return; 
        }
    }
    else if (retn.range < 0 || retn.range > cornerRange)
    { 
        return; 
    }
    
    float r;
    if (COLLAPSE_MIN_RANGE)
    {
        r = (retn.range - minRange) / (maxRange - minRange);
    } else
    {
        r = retn.range / maxRange;
    }
    
    r *= output->yHeight();
    theta += M_PI_2; // Rotate from aircraft azimuthal frame to display space
    int x = r * cos(theta) + output->xWidth() / 2;
    int y = r * sin(theta);
    /*theta *= 180. / M_PI;
    int y = r;
    int x = output.xWidth() - output.xWidth()*(theta - 30) / 120.0f;*/
    output->setPixel(
        x,
        y,
        Pixel(retn.power));
}

void RadarScope::clear()
{
    for (int r = 0; r < maxRangeSegments; r++)
    {
        for (int a = 0; a < pulsesPerSweep; a++)
        {
            pPArray[a * maxRangeSegments + r].area = 0.0f;
            pPArray[a * maxRangeSegments + r].value = 0.0f;
            pPArray[a * maxRangeSegments + r].nValues = 0;
        }
    }
}
