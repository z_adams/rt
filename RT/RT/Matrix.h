#pragma once

#ifndef ZMATRIX_H
#define ZMATRIX_H

#include "Vec3.h"
#include <iostream>

class Matrix4x4
{
private:
    float *matrix;
public:
    Matrix4x4();
    Matrix4x4(const Matrix4x4 &other);
    ~Matrix4x4();
    bool set(int row, int col, float value);
    void set(float v11, float v12, float v13, float v14,
             float v21, float v22, float v23, float v24,
             float v31, float v32, float v33, float v34,
             float v41, float v42, float v43, float v44);
    float get(int row, int col);
    float det() const;
    const Matrix4x4 transpose() const;
    static const Matrix4x4 computeRotationMatrix(float rX, float rY, float rZ);
    Matrix4x4& operator=(const Matrix4x4 &other);
    const Matrix4x4 operator+(const Matrix4x4 &rhs) const;
    const Matrix4x4 operator-(const Matrix4x4 &rhs) const;
    const Matrix4x4 operator*(const Matrix4x4 &rhs) const;
    const Matrix4x4 operator*(const float &rhs) const;
    const Vec4D_t operator*(const Vec4D_t &rhs) const;
    friend std::ostream& operator<<(std::ostream&, const Matrix4x4 &m);
};

#endif