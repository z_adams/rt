// RTExperiment.cpp : Defines the entry point for the console application.
//


#define _USE_MATH_DEFINES
#include <iostream>
#include <ctime>
#include "Ray.h"
#include "RadarScope.h"
#include "Radar.h"
#include "Initializer.h"

void rayTest();
void testTracing();
void testMapping();
void testRadar();
void testRadarMapping();
void testImageReading();
void profiler();
void octree();

// Camera coordinates: straight ahead == -Z, X & Y as cartesian pixel coords
// World coordinates: Z up, Y ahead, X transverse
// Test camera position: (0,0,3.0), Xrot = 60 deg

int main()
{
    //testTracing();
    testRadarMapping();
    //testMapping();
    //rayTest();
    //octree();
    std::cout << "\ndone\n";
}

//void testTracing()
//{
//    ObjParser op;
//    Camera cam(Vec3D_t(0.0f, 0.0f, 3.0f),
//        Vec3D_t(0.0f, 0.0f, -1.0f), 600, 600, 80, 24);
//    cam.computeMatrix(60.0f, 0.0f, 0.0f);
//    std::shared_ptr<Mesh> meshPtr = op.processMesh("../../models/radarPlayground.obj", "../../textures/radarPlaygroundTex.bmp");
//    meshPtr->setPosition(Vec3D_t(0.0f, 6.0f, 0.0f));
//    //cam.addObject(meshPtr);
//    std::clock_t start = std::clock();
//    cam.trace();
//    std::cout << "Ray tracing function finished tracing " << meshPtr->nFaces
//        << " triangles in " << (std::clock() - start) / 1000. << " seconds\n";
//    cam.print("../../radar.bmp");
//}

//void testRadar()
//{
//    ObjParser op;
//    RadarFunctions rd(
//        Vec3D_t(0.0f, 0.0f, 3.0f), 
//        Vec3D_t(0.0f, 1.0f, 0.0f), 
//        120, 
//        60.0f, 
//        30.0f, 
//        120.0f, 
//        3.0f, 
//        40.0f,
//        50,
//        240, 
//        480);
//    rd.setScanRange(30.0f);
//    Mesh *pM = op.processMesh("../../models/terrain_sm.obj", "../../textures/terrainTexExperiment.bmp");
//    pM->setPosition(Vec3D_t(0.0f, 10.0f, 0.0f));
//    rd.addObject(pM);
//    std::clock_t start = std::clock();
//    rd.sweep();
//    std::cout << "Ray tracing function finished tracing " << pM->nFaces
//        << " triangeles in " << (std::clock() - start) / 1000. << " seconds\n";
//    //rd.write();
//}

struct Point
{
    float x, y, z;
    Vec3D_t centroid() { return Vec3D_t(x, y, z); }
};
std::ostream& operator<<(std::ostream &out, Point pt)
{
    out << "(" << pt.x << ", " << pt.y << ", " << pt.z << ")";
    return out;
}

//void octree()
//{
//    Point points[8];
//    points[0] = { 1, 1, 1 };
//    points[1] = { 1, 1, 1.2 };
//    /*points[0] = { 1, 1, 1 };
//    points[1] = { -1, 1, 1 };
//    points[2] = { -1, -1, 1 };
//    points[3] = { 1, -1, 1 };
//    points[4] = { 1, 1, -1 };
//    points[5] = { -1, 1, -1 };
//    points[6] = { -1, -1, -1 };
//    points[7] = { 1, -1, 1 };*/
//    Zoctree<Point*, 1> tree(Vec3D_t(), 3, 3, 3);
//    /*for (int i = 0; i < 8; i++)
//    {
//        tree.insert(&points[i]);
//    }*/
//    tree.insert(&points[0]);
//    tree.insert(&points[1]);
//    Ray ray(Vec3D_t(1.0f, 1.0f, 5.0f), Vec3D_t(0.0f, 0.0f, -1.0f));
//    tree.findHighestIntersectingVolume(ray);
//    Volume<Point*, 1> *vol;
//    for (int i = 0; i < tree.numResults(); i++)
//    {
//        vol = tree.getResult(i);
//        if (!vol->leaf)
//        {
//            continue;
//        }
//        for (int j = 0; j < 1; j++)
//        {
//            std::cout << *vol->getData(j) << "\n";
//        }
//    }
//}

void rayTest()
{
    Ray ray(Vec3D_t(), Vec3D_t(0, 1, 0));
    Box box(Vec3D_t(0, 10, 0), 10, 10, 10);
    Vec3D_t ip;
    Vec2D_t uv;
    std::cout << box.intersectedBy(ray, 0, 100);
}

//void testMapping()
//{
//    float rangeSegments = 220;
//    float beamWidth = 2.0f;
//    int azimuthalResolution = 240;
//    std::unique_ptr<RadarScope> radarDisplay = std::unique_ptr<RadarScope>(
//        new RadarScope(960, 960, 24, azimuthalResolution, beamWidth, rangeSegments, 80));
//    bool color = true;
//    for (int i = 0; i < radarDisplay->pulsesPerSweep; i++)
//    {
//        for (int j = 0; j < radarDisplay->maxRangeSegments; j++)
//        {
//            radarDisplay->pPArray[i*radarDisplay->maxRangeSegments + j].value 
//                = (color) ? 1 : 0;
//            color = !color;
//        }
//        color = !color;
//    }
//    radarDisplay->mapImage();
//    radarDisplay->writeToFile("../../testPattern.bmp");
//}

void testRadarMapping()
{
    Initializer init("");

    std::cout << "Loading meshes:\n";
    init.sceneObjects.processMesh("../../models/terrainDEM.obj", "../../textures/terrainDEM.bmp");
    //init.sceneObjects.processMesh("../../models/scanFieldTest.obj", "../../textures/scanFieldTest.obj");
    init.sceneObjects.createObject(0, Vec3D_t(0.0f, 0.0f, 0.0f));
    std::cout << "Loading complete.\n";

    int numTris;
    RadarFunctions rd = init.build(&numTris);
    rd.setScanRange(40.0f);
    /*rd.setSweepAngle(20.0f);
    rd.setAntennaAzimuth(15.0f);*/

    std::clock_t start = std::clock();
    rd.sweep();
    std::cout << "Ray tracing function finished tracing " << numTris
        << " triangeles in " << (std::clock() - start) / 1000. << " seconds\n";
    rd.writeRaw("../../classRadarTest.bmp");
    start = std::clock();
    rd.rasterize();
    std::cout << "Mapping function finished in " << (std::clock() - start) << " ms\n";
    rd.write("../../rasterizedScope.bmp");
}

//void testImageReading()
//{
//    BmpImage img("../../checkerboard.bmp");
//    img.filepath = "../../checkerboard_out.bmp";
//    img.writeToFile();
//}

void profiler()
{
    float *resultA = new float[1000000];
    float *resultB = new float[1000000];
    std::clock_t start = std::clock();
    for (int i = 0; i < 1000; i++)
    {
        for (int j = 0; j < 1000; j++)
        {
            resultA[i*j] = sqrt(i * i + j * j);
        }
    }
    std::cout << "Loop completed in " << (std::clock() - start) << " ms";
    start = std::clock();
    for (int i = 0; i < 1000; i++)
    {
        for (int j = 0; j < 1000; j++)
        {
            resultB[i * j] = sqrt(i * i + j * j);
            resultA[i * j] = atan2(j, i);
        }
    }
    std::cout << "Loop completed in " << (std::clock() - start) << " ms";

    delete[] resultA;
    delete[] resultB;
}