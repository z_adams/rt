#pragma once

#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include <memory>
#include "Pixel.h"

#pragma pack(push, 1)
struct BMPFileHeader
{
    uint16_t filetype;
    uint32_t fileSize;
    uint16_t reserved1;
    uint16_t reserved2;
    uint32_t pxArrayOset;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct BMPInfoHeader
{
    uint32_t headerSize;
    uint32_t bmpWidth;
    uint32_t bmpHeight;
    uint16_t imgPlanes;
    uint16_t bitsPerPx;
    uint32_t compressionType;
    uint32_t bmpDataSizeBytes;
    uint32_t horizPxPerMeter;
    uint32_t vertPxPerMeter;
    uint32_t numColors;
    uint32_t numImportantColors;
};
#pragma pack(pop)

class BitMap
{
public:
    virtual bool setPixel(int x, int y, Pixel px) = 0;
    virtual Pixel getPixel(int x, int y) const = 0;
    virtual int bitsPerPx() const = 0;
    virtual int xWidth() const = 0;
    virtual int yHeight() const = 0;
    virtual void writeToFile(std::string filepath) = 0;
};

class BmpImage : public BitMap
{
private:
    std::string filepath;
    int bitsPerPixel, rows, rowWidth, bytesPerRow;
    BMPFileHeader fileHeader;
    BMPInfoHeader infoHeader;
    std::unique_ptr<Pixel[]> pixelArray;

    // Converts a 4-byte array into a single 4-byte int
    static uint32_t processBuffer(uint8_t *buffer);
public:
    // Construct an image object with the specified parameters
    BmpImage(std::string filepath, int xResolution,
        int yResolution, int bitsPerPixel);

    // Open an image at the specified filepath
    BmpImage(std::string filepath);

    // Create a default image "output.bmp", 512x512x24
    BmpImage() : BmpImage("output.bmp", 512, 512, 24) {}

    // Duplicate an image object
    BmpImage(BmpImage &other);

    // Duplicate an image object with a new name
    BmpImage(std::string filepath, BmpImage &other) : 
        BmpImage(filepath, other.rowWidth, other.rows, other.bitsPerPixel) {}

    bool setPixel(int x, int y, Pixel px)
    {
        if (x > rowWidth - 1 || y > rows - 1 || y < 0 || x < 0)
        {
            return false;
        }
        pixelArray.get()[y*rowWidth + x] = px;
        return true;
    }

    Pixel getPixel(int x, int y) const
    {
        if (x > rowWidth - 1 || y > rows - 1 || y < 0 || x < 0)
        {
            return Pixel();
        }
        return pixelArray.get()[y*rowWidth + x];
    }
    int bitsPerPx() const { return bitsPerPixel; }
    int xWidth() const { return rowWidth; }
    int yHeight() const { return rows; }
    std::string getFilePath() const { return filepath; }
    void readFile(std::ifstream &fs);
    void readData(std::ifstream &fs);
    void writeToFile();
    void writeToFile(std::string filepath);
};

#endif