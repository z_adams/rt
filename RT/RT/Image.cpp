#include "Image.h"
#include <fstream>
#include <cmath>

// File structure sourced from https://en.wikipedia.org/wiki/BMP_file_format

constexpr int OSET_FILESIZE     = 0x02;
constexpr int OSET_IMG_WIDTH    = 0x12;
constexpr int OSET_BIT_DEPTH    = 0x1C;
constexpr int OSET_BMP_DATA     = 0x36;

BmpImage::BmpImage(std::string filepath, int xResolution, int yResolution,
    int bitsPerPixel)
{
    this->filepath = filepath;
    rows = yResolution;
    rowWidth = xResolution;
    this->bitsPerPixel = 24;
    bytesPerRow = ceil((bitsPerPixel * rowWidth) / 32.0) * 4;
    pixelArray = std::unique_ptr<Pixel[]>(new Pixel[rows*rowWidth]);

    size_t pixelDataSize = rows * bytesPerRow;
    size_t fileSize = sizeof(BMPFileHeader) +
        sizeof(BMPInfoHeader) + pixelDataSize;

    fileHeader = {
        0x4D42,             // 'BM'
        (uint32_t)fileSize, // Size of the BMP file
        0x0,                // Unused (application specific)
        0x0,                // Unused (application specific)
        OSET_BMP_DATA       // Offset to beginning of pixel array
    };
    infoHeader = {
        0x28,               // Number of bytes in DIB header (from this pt)
        (uint32_t)rowWidth, // Width of bitmap in pixels
        (uint32_t)rows,     // Height of bitmap in pixels
        0x1,                // Number of color planes used
        (uint16_t)bitsPerPixel, // Number of bits per pixel
        0x0,                // BI_RGB, no pixel array compression used
        (uint32_t)pixelDataSize,// Size of the raw bitmap data (including padding)
        0x0B13,             // Horizontal print resolution (72DPI=2835px/m)
        0x0B13,             // Vertical print resolution (2835px/m)
        0x0,                // Colors in the palette
        0x0                 // 0 -> all colors are important
    };
}

BmpImage::BmpImage(std::string filepath) : 
    bitsPerPixel(0), fileHeader(), infoHeader(), rowWidth(0), rows(0)
{
    this->filepath = filepath;
    std::ifstream fs;
    readFile(fs);
    bytesPerRow = ceil((bitsPerPixel * rowWidth) / 32.0) * 4;
    pixelArray = std::unique_ptr<Pixel[]>(new Pixel[rows*rowWidth]);
    readData(fs);
    fs.close();
}

BmpImage::BmpImage(BmpImage &other)
{
    this->filepath = other.filepath;
    this->bitsPerPixel = other.bitsPerPixel;
    this->rows = other.rows;
    this->rowWidth = other.rowWidth;
    this->bytesPerRow = other.bytesPerRow;
    this->fileHeader = other.fileHeader;
    this->infoHeader = other.infoHeader;
    this->pixelArray = std::unique_ptr<Pixel[]>(new Pixel[rows*rowWidth]);
    for (int i = 0; i < rows*rowWidth; i++)
    {
        this->pixelArray.get()[i] = other.pixelArray.get()[i];
    }
}

void BmpImage::readFile(std::ifstream &fs)
{
    fs.clear();
    fs.seekg(0, std::ios::beg);
    uint8_t buffer[4] = {0};
    fs.open(filepath, std::ios::in | std::ios::binary);

    fs.seekg(OSET_FILESIZE);
    fs.read((char*)buffer, 4);
    uint32_t fileSize = processBuffer(buffer);

    fs.seekg(OSET_IMG_WIDTH);
    fs.read((char*)buffer, 4);
    rowWidth = processBuffer(buffer);

    fs.read((char*)buffer, 4);
    rows = processBuffer(buffer);

    fs.seekg(OSET_BIT_DEPTH);
    fs.read((char*)buffer, 4);
    bitsPerPixel = processBuffer(buffer);

    fileHeader = {
        0x4D42,
        fileSize,
        0x0,
        0x0,
        OSET_BMP_DATA
    };
    infoHeader = {
        0x28,
        (uint32_t)rowWidth,
        (uint32_t)rows,
        0x1,
        (uint16_t)bitsPerPixel,
        0x0,
        0x0,
        0x0B13,
        0x0B13,
        0x0,
        0x0
    };
}

void BmpImage::readData(std::ifstream &fs)
{
    fs.clear();
    fs.seekg(0, std::ios::beg);
    int paddingBytes = bytesPerRow - ((bitsPerPixel * rowWidth) / 8);
    fs.seekg(fileHeader.pxArrayOset);
    char r, g, b;
    for (int y = 0; y < rows; y++)
    {
        for (int x = 0; x < rowWidth; x++)
        {
            fs.get(r);
            fs.get(g);
            fs.get(b);
            pixelArray.get()[y*rowWidth + x] = Pixel(b, g, r);
        }
        fs.ignore(paddingBytes);
    }
}

// TODO: streamline storage and I/O of pixel data in memory
// definitely possible to substantially streamline
void BmpImage::writeToFile()
{
    std::ofstream fs;
    fs.open(filepath, std::ios::out | std::ios::binary);
    // write headers to file
    fs.write(reinterpret_cast<char*>(&fileHeader), sizeof(BMPFileHeader));
    fs.write(reinterpret_cast<char*>(&infoHeader), sizeof(BMPInfoHeader));

    char* buffer = new char[bytesPerRow];
    Pixel* pTmpPx;
    int paddingBytes = bytesPerRow - ((bitsPerPixel * rowWidth) / 8);
    for (int y = 0; y < rows; y++)  // for each pixel in the byte array
    {
        for (int x = 0; x < rowWidth; x++)  // insert contents of each pixel into byte array
        {
            pTmpPx = &pixelArray.get()[y*rowWidth + x];
            buffer[3 * x] = pTmpPx->b;
            buffer[3 * x + 1] = pTmpPx->g;
            buffer[3 * x + 2] = pTmpPx->r;
        }
        for (int k = 0; k < paddingBytes; k++)  // fill remaining bytes with 0x0 padding
        {
            buffer[bytesPerRow - paddingBytes + k] = 0x0;
        }
        fs.write(buffer, bytesPerRow);  // write contents of byte array buffer to file
    }
    fs.close();
}

void BmpImage::writeToFile(std::string filepath)
{
    std::string oldPath = this->filepath;
    std::string newPath = filepath;
    this->filepath = newPath;
    writeToFile();
    this->filepath = oldPath;
}

uint32_t BmpImage::processBuffer(uint8_t *buffer)
{
    return (buffer[0]) | (buffer[1]<<8) | (buffer[2]<<16) | (buffer[3]<<24);
}