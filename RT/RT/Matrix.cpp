#include "Matrix.h"

/* Matrix elements are stored in a 2D-indexed flat array
 * The packing arrangement is a row of columns, and thus is indexed in the form
 * A_ij = M[i*entriesPerRow + j] 
 * where A is the entry in the ith row & jth column
*/
Matrix4x4::Matrix4x4()
{
    matrix = new float[16];
    for (int i = 0; i < 16; i++)
    {
        matrix[i] = 0.0f;
    }
}

Matrix4x4::Matrix4x4(const Matrix4x4 &other)
{
    matrix = new float[16];
    for (int i = 0; i < 16; i++)
    {
        matrix[i] = other.matrix[i];
    }
}

Matrix4x4::~Matrix4x4()
{
    delete[] matrix;
}

bool Matrix4x4::set(int row, int col, float value)
{
    if (row > 3 || col > 3 || row < 0 || col < 0)
    {
        return false;
    }
    matrix[row*4 + col] = value;
    return true;
}

void Matrix4x4::set(
    float v11, float v12, float v13, float v14, 
    float v21, float v22, float v23, float v24, 
    float v31, float v32, float v33, float v34,
    float v41, float v42, float v43, float v44)
{
    matrix[0] = v11;
    matrix[0 + 1] = v12;
    matrix[0 + 2] = v13;
    matrix[0 + 3] = v14;

    matrix[1*4 + 0] = v21;
    matrix[1*4 + 1] = v22;
    matrix[1*4 + 2] = v23;
    matrix[1*4 + 3] = v24;

    matrix[2*4 + 0] = v31;
    matrix[2*4 + 1] = v32;
    matrix[2*4 + 2] = v33;
    matrix[2*4 + 3] = v34;

    matrix[3*4 + 0] = v41;
    matrix[3*4 + 1] = v42;
    matrix[3*4 + 2] = v43;
    matrix[3*4 + 3] = v44;
}

float Matrix4x4::get(int row, int col)
{
    if (row > 4 || col > 4 || row < 0 || col < 0)
        return NULL;
    //return matrix[row + 1][col + 1];
    return matrix[row * 4 + col];
}

float Matrix4x4::det() const
{
    return 0.0f;
}

const Matrix4x4 Matrix4x4::transpose() const
{
    Matrix4x4 output;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            output.matrix[j*4 + i] = matrix[i*4 + j];
        }
    }
    return output;
}

const Matrix4x4 Matrix4x4::computeRotationMatrix(float rX, float rY, float rZ)
{
    Matrix4x4 Rx, Ry, Rz, output;
    float radX = rX * (3.14159 / 180.),
        radY = rY * (3.14159 / 180.),
        radZ = rZ * (3.14159 / 180.);
    Rx.set(1,   0,          0,          0,
           0,   cos(radX),  -sin(radX), 0,
           0,   sin(radX),  cos(radX),  0,
           0,   0,          0,          1);

    Ry.set(cos(radY),   0, sin(radY),   0,
           0,           1, 0,           0,
           -sin(radY),  0, cos(radY),   0,
           0,           0, 0,           1);

    Rz.set(cos(radZ),   -sin(radZ), 0,  0,
           sin(radZ),   cos(radZ),  0,  0,
           0,           0,          1,  0,
           0,           0,          0,  1);

    output = Rz * Ry * Rx;
    return output;
}

Matrix4x4 & Matrix4x4::operator=(const Matrix4x4 & other)
{
    for (int i = 0; i < 16; i++)
    {
        this->matrix[i] = other.matrix[i];
    }
    return *this;
}

const Matrix4x4 Matrix4x4::operator+(const Matrix4x4 &rhs) const
{
    Matrix4x4 output;
    for (int i = 0; i < 16; i++)
    {
        output.matrix[i] = matrix[i] + rhs.matrix[i];
    }
    return output;
}

const Matrix4x4 Matrix4x4::operator-(const Matrix4x4 &rhs) const
{
    Matrix4x4 output;
    for (int i = 0; i < 16; i++)
    {
        output.matrix[i] = matrix[i] - rhs.matrix[i];
    }
    return output;
}

const Matrix4x4 Matrix4x4::operator*(const Matrix4x4 &rhs) const
{
    Matrix4x4 output;
    float dot;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            dot = 0;
            for (int k = 0; k < 4; k++)
            {
                dot += matrix[i*4 + k] * rhs.matrix[k*4 + j];
            }
            output.set(i, j, dot);
        }
    }
    return output;
}

const Matrix4x4 Matrix4x4::operator*(const float &rhs) const
{
    Matrix4x4 output;
    for (int i = 0; i < 16; i++)
    {
        output.matrix[i] = matrix[i] * rhs;
    }
    return output;
}

const Vec4D_t Matrix4x4::operator*(const Vec4D_t &rhs) const
{
    Vec4D_t result;
    result.x = 0;
    result.x += matrix[0 + 0] * rhs.x;
    result.x += matrix[0 + 1] * rhs.y;
    result.x += matrix[0 + 2] * rhs.z;
    result.x += matrix[0 + 3] * rhs.w;

    result.y = 0;
    result.y += matrix[1*4 + 0] * rhs.x;
    result.y += matrix[1*4 + 1] * rhs.y;
    result.y += matrix[1*4 + 2] * rhs.z;
    result.y += matrix[1*4 + 3] * rhs.w;

    result.z = 0;
    result.z += matrix[2*4 + 0] * rhs.x;
    result.z += matrix[2*4 + 1] * rhs.y;
    result.z += matrix[2*4 + 2] * rhs.z;
    result.z += matrix[2*4 + 3] * rhs.w;

    result.w = 0;
    result.w += matrix[3*4 + 0] * rhs.x;
    result.w += matrix[3*4 + 1] * rhs.y;
    result.w += matrix[3*4 + 2] * rhs.z;
    result.w += matrix[3*4 + 3] * rhs.w;

    return result;
}

std::ostream & operator<<(std::ostream &os, const Matrix4x4 &m)
{
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            os << m.matrix[i*4 + j] << "\t";
        }
        os << "\n";
    }
    return os;
}
