#define _USE_MATH_DEFINES
#include "Camera.h"

Camera::Camera(Vec3D_t origin, Vec3D_t direction, int xResolution,
    int yResolution, float foV, int colorBitDepth, float aspectRatio)
    : img("../../output.bmp", xResolution, yResolution, 24)
{
    this->origin = origin;
    this->direction = direction;
    this->xResolution = xResolution;
    this->yResolution = yResolution;
    this->imgPlaneDepth = 0.5f / tan((foV * (M_PI / 180.)) / 2);
    this->aspectRatio = 
        (aspectRatio) ? aspectRatio : (xResolution / yResolution);
    this->colorBitDepth = colorBitDepth;
    this->sceneObjectCount = 0;
    this->sceneObjectsCapacity = 1;
    //this->sceneObjects = new Mesh*[1];
}

Camera::Camera() : img()
{
    this->origin = Vec3D_t(0.0f, 0.0f, 0.0f);
    this->direction = Vec3D_t(0.0f, 0.0f, -1.0f);
    this->xResolution = 0;
    this->yResolution = 0;
    this->sceneObjectCount = 0;
    this->sceneObjectsCapacity = 1;
    imgPlaneDepth = 0.577f;
    this->colorBitDepth = 24;
    //this->sceneObjects = nullptr;
}

//Camera::~Camera()
//{
//    delete[] sceneObjects;
//}

Vec3D_t Camera::cameraToWorldCoords(const Vec3D_t &vector)
{
    Vec4D_t vec4(vector);
    vec4 = cameraMatrix * vec4;
    return Vec3D_t(vec4.x, vec4.y, vec4.z);
}

void Camera::computeMatrix(float rX, float rY, float rZ)
{
    cameraMatrix = Matrix4x4::computeRotationMatrix(rX, rY, rZ);
    std::cout << cameraMatrix;
}

Pixel Camera::castRay(const Vec3D_t &direction)
{
    Vec3D_t intersectPoint, faceNormal, vertNormal;
    Vec2D_t uv, texCoordinate;
    Ray ray(this->origin, direction);
    Face *currentFace, *pNearestFace = nullptr;
    Triangle tri;
    float depth = ray.tMax, norm;
    int value = 255, tU, tV;
    Pixel px;
    uint8_t r, g, b, c;
    Vec3D_t tricolor[3] = { {1,0,0}, {0,1,0}, {0,0,1} }, hitColor;
    //Mesh &mesh;

    /*for (int i = 0; i < sceneObjectCount; i++)
    {
        mesh = (sceneObjects)[i];
        for (int j = 0; j < mesh->nFaces; j++)
        {
            tri = mesh->faces[j].translate(mesh->getPosition());
            if (ray.intersect(
                ray.origin, ray.direction, tri, intersectPoint, uv))
            {
                norm = intersectPoint.norm();
                if (norm < depth)
                {
                    depth = norm;
                    pNearestFace = &mesh->faces[j];
                    mesh->getSurfaceProperties(intersectPoint, this->direction, 
                        *pNearestFace, uv, vertNormal, texCoordinate);
                    tU = (int)(texCoordinate.x * mesh->texture.xWidth());
                    tV = (int)(texCoordinate.y * mesh->texture.yHeight());
                    px = mesh->texture.getPixel(tU, tV);
                }
            }
        }
    }*/
    
    if (!pNearestFace)
    {
        return Pixel(0x0, 0x0, 0x0);
    }
    //c = static_cast<BYTE>(calculateReflectivity(vertNormal, direction, 1.0f, (float)((px.r - 0.3)/value)) * value);
    pNearestFace->tri.Barycentric(intersectPoint, hitColor.x, hitColor.y, hitColor.z);

    faceNormal = pNearestFace->tri.normal();
    c = static_cast<uint8_t>(vertNormal.dot(direction) * value);
    //r = static_cast<BYTE>(hitColor.x * value);
    //g = static_cast<BYTE>(hitColor.y * value);
    //b = static_cast<BYTE>(hitColor.z * value);
    //return Pixel(hitColor.x, hitColor.y, hitColor.z);
    //return px;
    return Pixel(c, c, c);
}


/*
Here for debugging purposes, so I can look visually at the mapping
*/
float Camera::calculateReflectivity(const Vec3D_t &hitNormal, const Vec3D_t &rayDirection, float impedance, float roughness) const
{
    /*float gamma;  // impedance isn't working yet
    if (impedance < 0.1)
    {
    gamma = 1.0f;
    } else
    {
    gamma = (impedance - impedanceFreeSpace) /
    (impedance + impedanceFreeSpace);
    }*/
    const float gamma = roughness;
    const float sigmaSqd = roughness * roughness,
        incdAngle = acos(hitNormal.dot(rayDirection * -1) / 2),
        a = 1 - 0.5f * ((sigmaSqd) / (sigmaSqd + 0.33f)),
        b = 0.45f * (sigmaSqd / (sigmaSqd + 0.09f)),
        sinAlpha = sin(incdAngle), tanBeta = tan(incdAngle);
    return (impedance / 3.14159f) * cos(incdAngle) * (a + (b*sinAlpha*tanBeta) * gamma);
}

/* Resizes the sceneObjects array
 * Use caution: truncates sceneObjects when downsized
*/
//void Camera::reallocateSceneObjects(const int newSize)
//{
//    if (newSize == sceneObjectsCapacity)
//    {
//        return;
//    }
//    Mesh **sceneObjectsBuffer = new Mesh*[newSize];
//    int i;
//    for (i = 0; i < newSize; i++)
//    {
//        sceneObjectsBuffer[i] = sceneObjects[i];
//        sceneObjects[i] = nullptr;
//    }
//    for (; i < sceneObjectCount; i++)
//    {
//        delete sceneObjects[i];
//    }
//    delete[] sceneObjects;
//    sceneObjects = sceneObjectsBuffer;
//    sceneObjectsBuffer = nullptr;
//}


// TODO: Consider merging with reallocateSceneObjects if this doesn't get called independently much
// TODO: test
//void Camera::condenseSceneObjects()
//{
//    for (int i = 0, j; i < sceneObjectCount; i++)
//    {
//        if (!sceneObjects[i])
//        {
//            for (j = i + 1; j < sceneObjectsCapacity; j++)
//            {
//                if (sceneObjects[j])
//                {
//                    sceneObjects[i] = sceneObjects[j];
//                    sceneObjects[j] = nullptr;
//                }
//            }
//        }
//    }
//}
/*
 * sceneObjects array start with just one slot for terrain.
 * If an additional mesh is added, it's assumed there will be multiple,
 * so the next size is 5, and then 2*n from then on
*/
//void Camera::addObject(Mesh *mesh)
//{
//    condenseSceneObjects();
//    if (sceneObjectCount + 1 > sceneObjectsCapacity)
//    {
//        int newSize;
//        if (sceneObjectsCapacity == 1)
//        {
//            newSize = 5;
//        } else
//        {
//            newSize = sceneObjectsCapacity * 2;
//        }
//        reallocateSceneObjects(newSize);
//    }
//    sceneObjects[sceneObjectCount] = mesh; // sceneObjs[n] stores sceneObj n+1
//    sceneObjectCount++;
//}

void Camera::trace()
{
    Vec3D_t pixelLocation;
    float xWidth = 1.0f / xResolution, yWidth = (1 / aspectRatio) / yResolution;
    int xhRes = xResolution / 2;
    int yhRes = yResolution / 2;
    for (int y = -yhRes; y < yhRes; y++)
    {
        for (int x = -xhRes; x < xhRes; x++)
        {
            pixelLocation.set(x * xWidth, y * yWidth, -imgPlaneDepth);
            this->img.setPixel(
                x + xhRes, 
                y + yhRes, 
                castRay(cameraToWorldCoords(pixelLocation.normalize())));
        }
    }
}