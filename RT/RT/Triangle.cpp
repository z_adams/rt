#include "Triangle.h"


bool intersect(const Ray ray, const Triangle &inTri, Vec3D_t& intersectPoint, Vec2D_t& uv)
{
    const Vec3D_t edge1 = inTri.vert1 - inTri.vert0;
    const Vec3D_t edge2 = inTri.vert2 - inTri.vert0;
    const Vec3D_t h = ray.direction.cross(edge2);
    const float a = edge1.dot(h);
    if (a > -Ray::EPSILON && a < Ray::EPSILON)
    {
        return false;
    }
    const float f = 1 / a;
    const Vec3D_t s = ray.origin - inTri.vert0;
    const float u = f * (s.dot(h));
    if (u < 0.0 || u > 1.0)
    {
        return false;
    }
    const Vec3D_t q = s.cross(edge1);
    const float v = f * ray.direction.dot(q);
    if (v < 0.0 || u + v > 1.0)
    {
        return false;
    }
    float t = f * edge2.dot(q);
    if (t < Ray::EPSILON)
    {
        return false;
    }
    intersectPoint = ray.origin + ray.direction * t;
    uv.x = u;
    uv.y = v;
    return true;
}