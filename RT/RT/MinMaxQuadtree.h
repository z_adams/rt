#pragma once

#ifndef MMQUADTREE_H
#define MMQUADTREE_H

#include <memory>
#include <vector>
#include "Mesh.h"
#include "Ray.h"
#include "Triangle.h"
#include <functional>

/* class Boundary represents a specialized box primitive with a square
 * base centered around the x y coordinate of the box, with separate top/bottom
 * bounds which are free to be at any z position.
 */
class Boundary
{
public:
    Vec3D_t position;
    float xyWidth, zMax, zMin;
    Boundary(Vec3D_t position, float xyWidth,
        float zMax, float zMin)
        : position(position), xyWidth(xyWidth), zMax(zMax), zMin(zMin) {}
    Boundary(Vec3D_t position, float xyWidth)
        : position(position), xyWidth(xyWidth), zMax(0), zMin(0) {}
    bool contains(Vec3D_t point) const;
    float zMaxBound() const { return position.z + zMax; }
    float zMinBound() const { return position.z - zMin; }
    float xMaxBound() const { return position.x + (xyWidth / 2.0f); }
    float xMinBound() const { return position.x - (xyWidth / 2.0f); }
    float yMaxBound() const { return position.y + (xyWidth / 2.0f); }
    float yMinBound() const { return position.y - (xyWidth / 2.0f); }
    bool intersectedBy(const Ray ray, float t0, float t1) const;
};

template<typename T, int S>
class MinMaxQuadtree;

template <typename T, int S>
class Bin : public Boundary
{
    friend class MinMaxQuadtree<T, S>;
private:
    typedef typename std::vector<T*>::const_iterator data_itr;
    std::vector<T*> data;
    std::unique_ptr<Bin<T, S>> children[4];
    Bin<T, S> *parent;
    bool leaf;
public:
    Bin(Bin<T, S> *parent, Vec3D_t position, float xyWidth)
        : parent(parent), leaf(true),
        Boundary(position, xyWidth) {}
    Bin(Bin<T, S> *parent, Vec3D_t position, float xyWidth, float zMax, 
        float zMin) 
        : parent(parent), leaf(true), 
        Boundary(position, xyWidth, zMax, zMin) {}
    int quadrantIndex(Vec3D_t point) const;
    bool subdivide();
    std::unique_ptr<Bin<T, S>> generate(int quadrant);
    
    bool addData(T *data);
    void adjustZBounds(T *data);
    int findOverlaps(T *data);
    data_itr begin() const { return data.begin(); }
    data_itr end() const { return data.end(); }
};

/* The min-max quadtree is a 2.5D data structure for storing bounding volumes
 * containing terrain data that is guaranteed to be confined between an upper
 * and lower bound, such as terrain generated from a heightmap.
 * Its sub-elements contain non-owning pointers to the data elements of type T,
 * subdividing itself to keep the number of elements per volume to less than S.
 */
template <typename T, int S>
class MinMaxQuadtree
{
private:
    typedef std::vector<Bin<T, S>*> bin_buffer;
    typedef typename bin_buffer::const_iterator buffer_itr;
    std::unique_ptr<Bin<T, S>> HEAD;
    bin_buffer searchResults;
    void narrowIntersection(Ray ray, Bin<T, S> *current);
    void insert(T *object, Bin<T, S> *current);
public:
    MinMaxQuadtree(Vec3D_t position) 
        : HEAD(new Bin<T, S>(nullptr, position, 0)) {}
    MinMaxQuadtree() : MinMaxQuadtree(Vec3D_t()) {}
    void loadData(const Mesh &mesh);
    void search(Ray ray);
    void traverse();
    void traverse(Bin<T, S> *head);
    buffer_itr begin() const { return searchResults.begin(); }
    buffer_itr end() const { return searchResults.end(); }
};

template<typename T, int S>
inline int Bin<T, S>::quadrantIndex(Vec3D_t point) const
{
    if (point.x > position.x) 
    {
        if (point.y > position.y)
            return 0;
        return 3;
    } else 
    {
        if (point.y > position.y)
            return 1;
        return 2;
    }
}

template<typename T, int S>
inline bool Bin<T, S>::subdivide()
{
    if (!leaf)
        return false;
    for (data_itr itr = data.begin(); itr != data.end(); itr++)
    {
        int quadrant = quadrantIndex((*itr)->centroid());
        if (!children[quadrant])
        {
            children[quadrant] = generate(quadrant);
        }
        children[quadrant]->adjustZBounds(*itr);
        children[quadrant]->addData(*itr);
        
        //int overlaps = findOverlaps(*itr);
        //// future optimization: only loop once if olaps is a power of 2
        ////if (overlaps && !(overlaps & (overlaps - 1)))

        ////for (int quadrant = 0, mask = 0x1; mask < 0b1000; mask <<= 1, quadrant++)
        //for (int quadrant = 0, mask = 0x1; quadrant < 4; mask = 0x1 << ++quadrant)
        //{
        //    if (overlaps & mask)
        //    {
        //        if (!children[quadrant])
        //            children[quadrant] = generate(quadrant);
        //        children[quadrant]->adjustZBounds(*itr);
        //        children[quadrant]->addData(*itr);
        //    }
        //}
    }
    this->leaf = false;
    this->data.clear();
    return true;
}

template<typename T, int S>
inline std::unique_ptr<Bin<T, S>> Bin<T, S>::generate(int quadrant)
{
    float newWidth = Boundary::xyWidth / 2;
    float halfWidth = newWidth / 2;
    Vec3D_t newPosition;
    if (quadrant == 0 || quadrant == 3)
    {
        newPosition.x = Boundary::position.x + halfWidth;
        if (quadrant == 0)
            newPosition.y = Boundary::position.y + halfWidth;
        else
            newPosition.y = Boundary::position.y - halfWidth;
    } else
    {
        newPosition.x = Boundary::position.x - halfWidth;
        if (quadrant == 1)
            newPosition.y = Boundary::position.y + halfWidth;
        else
            newPosition.y = Boundary::position.y - halfWidth;
    }
    /* Note that zMin and zMax are flipped so that the bin will be later fit 
       to the data and not forced to include z = 0
    */
    return std::unique_ptr<Bin<T, S>>(
        new Bin<T, S>(this, newPosition, newWidth, zMin, zMax));
}

template<typename T, int S>
inline bool Bin<T, S>::addData(T *data)
{
    if (this->data.size() >= S)
        return false;
    this->data.push_back(data);
    return true;
}

template<typename T, int S>
inline void Bin<T, S>::adjustZBounds(T *data)
{
    /*if (data->centroid().z > Boundary::zMax)
        Boundary::zMax = data->centroid().z + 1e-3f;
    if (data->centroid().z < Boundary::zMin)
        Boundary::zMin = data->centroid().z - 1e-3f;*/
    if (data->maxZ() > Boundary::zMax)
        Boundary::zMax = data->maxZ();
    if (data->minZ() < Boundary::zMin)
        Boundary::zMin = data->minZ();
}

/* TODO: this is designed for square-square overlap, general solution would be
   designed for square-triangle to be truly accurate*/ 
template<typename T, int S>
inline int Bin<T, S>::findOverlaps(T *data)
{
    int flags = 0x0;
    //float maxX, maxY, minX, minY;
    float halfWidth = xyWidth / 2.0f;
    /*for (int i = 0; i < 4; i++)
    {
        minY = children[i]->position.y - quarterWidth;
        maxY = children[i]->position.y + quarterWidth;
        minX = children[i]->position.x - quarterWidth;
        maxX = children[i]->position.x + quarterWidth;

        if (data->maxX() >= minX && data->minX() <= maxX
            && data->maxY() >= minY && data->minY() <= maxY)
        {
            flags |= 0x1 << i;
        }
    }*/
    if (data->minX() > position.x + halfWidth
        || data->maxX() < position.x - halfWidth
        || data->minY() > position.y + halfWidth
        || data->maxY() < position.y - halfWidth)
    {
        // Region does not contain the data
        return 0x0;
    }
    if (data->maxX() >= position.x) // overlaps right side
    {
        if (data->maxY() >= position.y) // overlaps top
            //flags |= 0x0001;
            flags |= 0b0001;  // 0th
        if (data->minY() <= position.y) // overlaps bottom
            //flags |= 0x1000;
            flags |= 0b1000;  // 3rd
    } 
    if (data->minX() <= position.x) // overlaps left side
    {
        if (data->maxY() >= position.y) // overlaps top
            //flags |= 0x0010;
            flags |= 0b0010;  // 1st
        if (data->minY() <= position.y) // overlaps bottom
            //flags |= 0x0100;
            flags |= 0b0100;  // 2nd
    }
    return flags;
}

template<typename T, int S>
inline void MinMaxQuadtree<T, S>::narrowIntersection(Ray ray, Bin<T, S> *current)
{
    for (int i = 0; i < 4; i++)
    {
        if (!current->children[i])
            continue;
        if (current->children[i]->intersectedBy(ray, ray.tMin, ray.tMax))
        {
            if (current->children[i]->leaf)
            {
                searchResults.push_back(current->children[i].get());
            } else
            {
                narrowIntersection(ray, current->children[i].get());
            }
        }
    }
}

template<typename T, int S>
inline void MinMaxQuadtree<T, S>::insert(T *object, Bin<T, S> *current)
{
    // Must recursively expand all parents to ensure inclusion of children
    current->adjustZBounds(object);
    if (current->leaf)
    {
        if (current->data.size() < S)
        {
            // candidate leaf found
            current->addData(object);
            return;
        }
        if (current->data.size() >= S)
        {
            // candidate leaf full
            current->subdivide();
        }
    }
    int quadrant = current->quadrantIndex(object->centroid());
    if (current->children[quadrant] == nullptr)
    {
        current->children[quadrant] = current->generate(quadrant);
    }
    return insert(object, current->children[quadrant].get());
    //int overlaps = current->findOverlaps(object);
    /*for (int quadrant = 0x1; quadrant < 0b1000; quadrant <<= 1)
    {
        if (overlaps & quadrant)
        {
            if (!current->children[quadrant])
                current->children[quadrant] = current->generate(quadrant);
            insert(object, current->children[quadrant].get());
        }
    }*/

    /*for (int quadrant = 0, mask = 0x1; quadrant < 4; mask = 0x1 << ++quadrant)
    {
        if (overlaps & mask)
        {
            if (!current->children[quadrant])
                current->children[quadrant] = current->generate(quadrant);
            insert(object, current->children[quadrant].get());
        }
    }*/
}

template<typename T, int S>
inline void MinMaxQuadtree<T, S>::loadData(const Mesh &mesh)
{
    std::vector<SceneObject>::const_iterator itr;
    //Mesh *mesh = mgr.sceneMeshes[ObjectManager::TERRAIN_INDEX];
    
    /* Make no assumption about starting position: object may not be centered
     * at origin, therefore starting from an arbitrary vertex, we grow bounds
     * from that point and then calculate the optimal origin position for the
     * data structure
    */
    float xMax = mesh.vertices[0].x;
    float xMin = xMax;
    float yMax = mesh.vertices[0].y;
    float yMin = yMax;
    float zMax = mesh.vertices[0].z;
    float zMin = zMax;
    Vec3D_t origin;

    for (int i = 1; i < mesh.nVerts; i++)
    {
        if (mesh.vertices[i].x > xMax)
        {
            xMax = mesh.vertices[i].x;
        } else if (mesh.vertices[i].x < xMin)
        {
            xMin = mesh.vertices[i].x;
        }
        if (mesh.vertices[i].y > yMax)
        {
            yMax = mesh.vertices[i].y;
        } else if (mesh.vertices[i].y < yMin)
        {
            yMin = mesh.vertices[i].y;
        }
        if (mesh.vertices[i].z > zMax)
        {
            zMax = mesh.vertices[i].z;
        } else if (mesh.vertices[i].z < zMin)
        {
            zMin = mesh.vertices[i].z;
        }
    }
    /*zMin -= 1e-3f;
    zMax += 1e-3f;*/
    origin.x = (xMax + xMin) / 2.0f;
    origin.y = (yMax + yMin) / 2.0f;
    origin.z = (zMax + zMin) / 2.0f;
    float xWidth = xMax - xMin;
    float yWidth = yMax - yMin;
    HEAD->xyWidth = (xWidth > yWidth) ? xWidth : yWidth;
    HEAD->position = origin;
    HEAD->zMax = zMax;
    HEAD->zMin = zMin;

    // TODO: flawed model using centroids
    // May have to eventually put tris in multiple bins to not miss them
    for (int i = 0; i < mesh.nFaces; i++)
    {
        insert(&mesh.faces[i], HEAD.get());
    }
}

template<typename T, int S>
inline void MinMaxQuadtree<T, S>::search(Ray ray)
{
    searchResults.clear();
    if (HEAD->intersectedBy(ray, ray.tMin, ray.tMax))
    {
        if (HEAD->leaf)
            searchResults.push_back(HEAD.get());
        else 
            narrowIntersection(ray, HEAD.get());
    }
}

// Temporary
template<typename T, int S>
inline void MinMaxQuadtree<T, S>::traverse()
{
    traverse(HEAD.get());
}

template<typename T, int S>
inline void MinMaxQuadtree<T, S>::traverse(Bin<T, S> *head)
{
    /*if (!head->leaf)
        std::cout << "(" << head->position.x << ", " << head->position.y << ", "
        << head->position.z << ") : [" << head->xyWidth << ", " << head->zMax
        << ", " << head->zMin << "]\n";*/
    for (int i = 0; i < 4; i++)
    {
        if (head->children[i])
            traverse(head->children[i].get());
    }
    
}

#endif