#pragma once

#ifndef OBJMANAGER_H
#define OBJMANAGER_H

#include "Mesh.h"
#include "Vec3.h"
#include "Ray.h"
#include "ObjParser.h"
#include "Octree.h"
#include "MinMaxQuadtree.h"

// Stores the information relevant to a ray-triangle intersection
struct RayIntersection
{
    RayIntersection() : intersectPoint(), hitFace(nullptr), 
        hitObject(nullptr), hitMesh(nullptr), uv(), depth(-1) {}
    Vec3D_t intersectPoint;
    const Face *hitFace;
    SceneObject *hitObject;
    Mesh *hitMesh;
    Vec2D_t uv;
    float depth;
    operator bool()
    {
        return (bool)hitObject;
    }
};

// Responsible for managing all scene objects and geometry
class ObjectManager
{
private:
    std::unique_ptr<Mesh[]> sceneMeshes;
    // TODO: eventually separate current objects from master list
    std::unique_ptr<SceneObject[]> sceneObjects;
    Zoctree<Face*, 100> terrainOctree;
    //MinMaxQuadtree<Face, 1000> terrainOctree;
    int nObjects;
    static const int TERRAIN_INDEX;
public:
    ObjectManager(ObjParser &buffer);
    // Perform a brute force ray-tri intersection search of all scene geometry
    RayIntersection probe(Ray ray) const;
    // Perform a ray-tri intersection search of the sparse voxel octree
    RayIntersection probeTree(Ray ray);
    int getTotalSceneTris(bool visibleOnly = true);
};
#endif // !OBJMANAGER_H