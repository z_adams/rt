#include "Initializer.h"
#include "Config.h"

Initializer::Initializer(std::string config, std::shared_ptr<BitMap> output)
{
    // Initializing dependencies from Config.h
    using namespace RTConfig;

    radarData = std::unique_ptr<RadarData>(new RadarData(
        position, boreline, sweepAngle, antennaAzmLimit, bottomElevLimit,
        topElevLimit, beamWidth, fanVertWidth, rangeResolution));

    /*output("../../rawOutput.bmp", displayXRes, displayYRes, colorDepth),
    rasterizedOutput("../../scopeOutput.bmp", output),*/
    /*std::shared_ptr<BitMap> output(new BmpImage(
        "../../rawOutput.bmp", displayXRes, displayYRes, colorDepth));*/
    if (!output)
    {
        output = std::shared_ptr<BitMap>(new BmpImage(
            "../../scopeOutput.bmp", displayXRes, displayYRes, colorDepth));
    }
    
    radarDisplay = std::unique_ptr<RadarScope>(
        new RadarScope(std::move(output), azimuthRes, 
            azimuthalBeamWidth, rangeSegments, rangeLimit));
    /*radarDisplay = std::unique_ptr<RadarScope>(new RadarScope(
        displayXRes, displayYRes, colorDepth, azimuthRes, azimuthalBeamWidth,
        rangeSegments, rangeLimit));*/
}

RadarFunctions Initializer::build(int *sceneTris)
{
    std::shared_ptr<ObjectManager> scene(new ObjectManager(sceneObjects));
    *sceneTris = scene->getTotalSceneTris();
    return RadarFunctions(std::move(radarData), std::move(radarDisplay), std::move(scene));
}
