#define _USE_MATH_DEFINES
#include "Radar.h"
#include "Ray.h"

//TODO: figure out why surface diffusion isn't working

float rad(float degrees)
{
    return degrees * (M_PI / 180.0f);
}

/* Takes spherical coordinates and transforms them to cartesian coordinates.
 * Follows typical physics standard where r is the radius, theta is polar 
 * angle, and phi is azimuth, where theta->(0,pi), and phi->(0,2pi). Theta = 0
 * points in the +Z direction, theta = pi points in the -Z direction, and phi 
 * is the angle in the unit circle on the X-Y plane with phi=0 being the X axis
*/
Vec3D_t RadarFunctions::sphericalToCartesian(
    const float r, const float phi, const float theta)
{
    return Vec3D_t(
        r*sin(theta)*cos(phi),
        r*sin(theta)*sin(phi),
        r*cos(theta));
}

/* This function's job is to take the point hit by the ray and compute
 * the appropriate shading
*/
RadarReturn RadarFunctions::castDepthRay(const Vec3D_t &direction) const
{
    Ray ray(this->data->position, direction);
    Face *currentFace = nullptr, *pNearestFace = nullptr;
    Triangle tri;
    float depth = ray.tMax, norm = 0, dielectricCoeff = 0, roughness = 0;
    int value = 0xFF;
    int texIndexU = 0, texIndexV = 0;
    uint8_t r = 0, g = 0, b = 0, texValue = 0, returnStrength = 0;
    Mesh *pMesh = nullptr;
    Pixel px;

    /* The scene is probed by a blackbox function which provides the necessary 
     * information to compute the surface properties (the intersected face,
     * barycentric coordinates of intersect point), and a pointer to the hit
     * mesh. This information is used to extract the correct value from the
     * texture and perform the necessary shading 
    */
    // TODO: add method as an option somewhere
    //RayIntersection intersect = sceneObjects->probe(ray);     //use old method
    RayIntersection intersect = sceneObjects->probeTree(ray);   // accelerated method
    if (!intersect)
    {
        return RadarReturn(0x0, 0.0f);
    }
    SurfaceProperties surface = intersect.hitMesh->getSurfaceProperties(
        intersect.intersectPoint, direction, *intersect.hitFace, intersect.uv);
    // Change to pixel space for texel access
    texIndexU = (int)(
        surface.texCoordinates.x * intersect.hitMesh->texture->xWidth());
    texIndexV = (int)(
        surface.texCoordinates.y * intersect.hitMesh->texture->yHeight());
    px = intersect.hitMesh->texture->getPixel(texIndexU, texIndexV);

    // Reversed polarity so that white on map means dark (reflective)
    roughness = 1.0f - (float)(px.r)/value;
    dielectricCoeff = 1.0f; // ((float)(texValue) / value) * 2.5f + 2.5f; // albedo, go between 2.5 and 5.0
    returnStrength = static_cast<uint8_t>(calculateReflectivity(
        surface.hitNormal, direction, dielectricCoeff, roughness) * value);
    //faceNormal = pNearestFace->tri.normal();
    //BYTE c = static_cast<BYTE>(vertNormal.dot(direction * -1) * value);
    /*r = static_cast<BYTE>(faceNormal.x * value);
    g = static_cast<BYTE>(faceNormal.y * value);
    b = static_cast<BYTE>(faceNormal.z * value);*/
    //return RadarReturn(0xFF, 0xFF, 0xFF, depth);//0xFF, 0xFF, 0xFF, depth);
    float dot = fabs(surface.hitNormal.dot(ray.direction * 2));  // TODO: to make things brighter; need better reflection model
    return RadarReturn(px.r*dot, intersect.depth);
}

/* Performs the main sweep of the radar
 * Loops over elevation (outer loop), then azimuth (inner loop) to produce the
   combined effect of sweeping out circles on the ground of increasing diameter
 * Maps returns from each raycast to the display output
 * Sweeps from left to right, with angles given on the unit circle centered at
   the location of the emitter with angles increasing in the positive direction
*/
void RadarFunctions::radarTrace()
{
    Vec3D_t pixelLocation;
    RadarReturn retn;
    float currentRange = display->getMinRange(), angle = 0;
    int row = 0, maxRows = 416;  // TMP: for calculating progress
    float percent = 0, da = 0;
    float vehicleAzimuth = atan2(data->boreline.y, data->boreline.x);
    // Compute starting and ending elevations and azimuths.
    float startElev = M_PI - rad(data->antennaElev - (data->fanVertWidth / 2));
    float stopElev = M_PI - rad(data->antennaElev + (data->fanVertWidth / 2));
    float startAzm = rad(data->azimuth + data->sweepAngle / 2);
    float stopAzm = rad(data->azimuth - data->sweepAngle / 2);
    const bool debugAvailable = display->debugOutputAvailable();
    for (float b = startElev; b > stopElev; b = M_PI - angle)
    {
        row++;
        if (((float)row / maxRows) > (percent + 0.1))
        {
            percent += 0.1f;
            std::cerr << "\rProgress: " << percent;
        }
        // Calculate the depression angle corresponding to the next arc of
        // points on the ground to achieve even spacing
        currentRange += data->pointResolution;
        angle = atan(currentRange / data->position.z);
        da = atan(data->pointResolution / currentRange);

        for (float a = startAzm; a > stopAzm; a -= da)
        {
            // TODO: consider converting entire system to spherical coordinates
            pixelLocation = sphericalToCartesian(1.0f, a + vehicleAzimuth, b);
            retn = castDepthRay(pixelLocation);
            display->mapPixelSpherical(a, retn);
            if (debugAvailable)
                display->mapPoint(a, retn);  // TMP: for debugging point cloud
        }
    }
    std::cerr << "\nrows:" << row << "\n";
}

/* Oren-Nayar physically based reflectivity
 * sigma/roughness is the standard deviation of the angle between the
 * microfacet normals and the macroscopic normal.
 * - Naty Hoffman & Dan Baker, siggraph06
 * Gamma (reflection constant) is calculated from the impedances
 * of the boundary materials (i.e air and stone)
 * Impedance Z = sqrt(abs magnetic permeability / abs electric permittivity)

 * Impedance of a few materials:
 * https://www.ndt.net/article/ndtce03/papers/v078/v078.htm
 * permittivity of free space: 8.854187e-12
 * Concrete: rel. permea. = 1.0 (non-magnetic) 
             rel. permit. = 6 (dry) - 12 (saturated)
             Z = 154 Ohm
 * Water: abs permea. = 1.256 627e-6
          rel permit. = ~60, 20C 1% saline
          abs permit. = 5.312 512e-10
          Z = 48 Ohm
 * Dielectric constants:
 * http://gprrental.com/gpr-velocity-table-analysis/
 * Loamy soil dry/wet: 2.5/19
 * Granite dry/wet: 5/7
 * Limestone dry/wet: 7/8
 * Sand dry/wet: 3-6/20-30
 * 
 */
float RadarFunctions::calculateReflectivity(
    const Vec3D_t &hitNormal, const Vec3D_t &rayDirection, 
    float RDEC, float roughness) const
{
    /*
    * For low-loss/non-magnetic materials:
    * Z ~ 377/sqrt(dielectric const)
    * er1, er2 = relative dielectric const of material 1, 2
    * Therefore gamma = (sqrt(er1) - sqrt(er2))/(sqrt(er1) + sqrt(er2))
    */
    //float gamma;  // impedance isn't working yet
    //if (RDEC < 0.1)
    //{
    //    gamma = 1.0f;
    //} else
    //{
    //    gamma = (RDEC - impedanceFreeSpace) /
    //        (RDEC + impedanceFreeSpace);
    //}
    const float gamma = roughness;
    const float sigmaSqd = roughness * roughness;
    const float incdAngle = acos(hitNormal.dot(rayDirection * -1) / 2);
    const float a = 1 - 0.5f * ((sigmaSqd) / (sigmaSqd + 0.33f));
    const float b = 0.45f * (sigmaSqd / (sigmaSqd + 0.09f));
    const float sinAlpha = sin(incdAngle);
    const float tanBeta = tan(incdAngle);
    return (RDEC / M_PI) * cos(incdAngle) 
        * (a + (b*sinAlpha*tanBeta) * gamma);
}

bool RadarFunctions::setSweepAngle(float sweepAngle)
{
    if (sweepAngle > 2 * data->antennaAzmLimit)
        return false;
    data->sweepAngle = sweepAngle;
    return true;
}

bool RadarFunctions::setAntennaAzimuth(float azimuth)
{
    if ((abs(azimuth) + data->sweepAngle / 2) > data->antennaAzmLimit)
        return false;
    data->azimuth = -azimuth;  // Flip from aeronautical azimuth to polar
    return true;
}

bool RadarFunctions::setAntennaElevation(float antennaElev)
{
    if (antennaElev < data->bottomElevLimit || antennaElev > data->topElevLimit)
        return false;
    data->antennaElev = antennaElev;
    return true;
}

bool RadarFunctions::setScanRange(float range)
{
    float cornerRange = display->computeCornerRange(range);
    float angleOfFarLimit = atan(cornerRange / data->position.z) * (180 / M_PI);
    setAntennaElevation(angleOfFarLimit - data->fanVertWidth / 2);
    float minRange = data->position.z * tan(RadarScope::rad(angleOfFarLimit - data->fanVertWidth));
    display->changeRangeLimits(minRange, range);
    return true;
}

RadarData::RadarData(
    Vec3D_t position, 
    Vec3D_t boreline, 
    float sweepAngle, 
    float antennaAzmLimit, 
    float bottomElevLimit, 
    float topElevLimit, 
    float beamWidth, 
    float fanVertWidth, 
    float rangeResolution)
{
    this->position = position;
    this->boreline = boreline;
    this->sweepAngle = sweepAngle;
    this->antennaAzmLimit = antennaAzmLimit;
    this->bottomElevLimit = bottomElevLimit;
    this->topElevLimit = topElevLimit;
    this->beamWidth = beamWidth; // unused
    this->fanVertWidth = fanVertWidth;
    this->pointResolution = 0.1f; //0.063f;
    this->rangeResolution = rangeResolution;  // unused
    this->horizResolution = 0.0f;  // unused
    this->azimuth = 0.0f;
    this->antennaElev = topElevLimit;
}
