#pragma once

#ifndef PIXEL_H
#define PIXEL_H

//#include <Windows.h>
#include <cstdint>

struct Pixel
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    Pixel(uint8_t red, uint8_t green, uint8_t blue)
        : r(red), g(green), b(blue) {}
    Pixel(uint8_t value) : r(value), g(value), b(value) {}
    Pixel() : r(0x0), g(0x0), b(0x0) {}
};

#endif