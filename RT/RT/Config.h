#pragma once

#ifndef RT_CONFIG_H
#define RT_CONFIG_H

#include "Vec3.h"

namespace RTConfig
{
    Vec3D_t position = Vec3D_t(0.0f, 0.0f, 3.0f);
    Vec3D_t boreline = Vec3D_t(0.0f, 1.0f, 0.0f); // (E, N, U)
    float sweepAngle = 120.0f;
    float antennaAzmLimit = 60.0f;
    float bottomElevLimit = 30.0f;
    float topElevLimit = 120.0f;
    float beamWidth = 2.0f;
    float fanVertWidth = 40.0f;
    float rangeResolution = 280.0f;

    int displayXRes = 480;
    int displayYRes = 480;
    int colorDepth = 24;
    int azimuthRes = 240;
    float azimuthalBeamWidth = beamWidth;
    int rangeSegments = 280;
    float rangeLimit = 80.0f;
}


#endif
