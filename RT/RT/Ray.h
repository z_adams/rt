#pragma once

#ifndef RAY_H
#define RAY_H

#include "Vec3.h"

class Ray
{
public:
    static constexpr float EPSILON = 0.0000001f;
    Vec3D_t origin, direction, inverseDir;
    float tMin, tMax;

    Ray(const Vec3D_t &origin, const Vec3D_t &direction) :
        origin(origin), direction(direction), tMin(0.0f), tMax(80.0f) {}
    Ray() { Ray(Vec3D_t(0, 0, 0), Vec3D_t(0, 0, -1.0f)); }
    Vec3D_t computeInvDir() const
    {
        return Vec3D_t(1 / direction.x, 1 / direction.y, 1 / direction.z);
    }
};

#endif
