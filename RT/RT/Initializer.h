#pragma once

#ifndef RADAR_INIT_H
#define RADAR_INIT_H

#include <memory>
#include "Radar.h"
#include "RadarScope.h"
#include "ObjParser.h"

class Initializer
{
private:
    std::unique_ptr<RadarData> radarData;
    std::unique_ptr<RadarScope> radarDisplay;
public:
    ObjParser sceneObjects; // exposed so that scene geometry can be added

    Initializer(std::string config, std::shared_ptr<BitMap> output = nullptr);
    RadarFunctions build(int *sceneTris = nullptr);
};

#endif