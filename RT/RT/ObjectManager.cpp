#include "ObjectManager.h"
#include <functional>

const int ObjectManager::TERRAIN_INDEX = 0;

ObjectManager::ObjectManager(ObjParser &buffer)
{
    sceneMeshes = std::unique_ptr<Mesh[]>(new Mesh[buffer.numMeshes]);
    sceneObjects = 
        std::unique_ptr<SceneObject[]>(new SceneObject[buffer.numObjects]);

    for (int i = 0; i < buffer.numMeshes; i++)
    {
        sceneMeshes[i] = *buffer.meshes[i];
    }

    SceneObject tmpObj;
    nObjects = 0;
    for (int i = 0; i < buffer.numObjects; i++)
    {
        tmpObj = buffer.objects[i];
        tmpObj.mesh = &sceneMeshes[tmpObj.meshID];
        sceneObjects[i] = tmpObj;
        nObjects++;
    }
    float widthX, widthY, height;
    Vec3D_t center;

    // OCTREE
    sceneMeshes[TERRAIN_INDEX].bounds(&center, &widthX, &widthY, &height);
    terrainOctree = Zoctree<Face *, 100>(center, widthX, widthY, height);
    for (int i = 0; i < sceneMeshes[TERRAIN_INDEX].nFaces; i++)
    {
        terrainOctree.insert(&sceneMeshes[TERRAIN_INDEX].faces[i]);
    }

    // MINMAX
    /*terrainOctree = MinMaxQuadtree<Face, 1000>(center);
    terrainOctree.loadData(sceneMeshes[TERRAIN_INDEX]);*/
}

/* Probes the scene and provides the necessary information to compute the 
 * surface properties (the intersected face, barycentric coordinates of 
 * intersect point), and a pointer to the hit mesh.
*/
RayIntersection ObjectManager::probe(Ray ray) const
{
    RayIntersection rayIntersection;
    Vec3D_t intersectPoint, finalIntersectPoint;
    float depth = ray.tMax;
    bool faceWasHit = false;
    int objIndex = -1, faceIndex = -1;
    Vec2D_t uv, hitUV;
    Triangle tri;
    Vec3D_t origin = ray.origin;
    for (int i = 0; i < nObjects; i++)
    {
        if (!sceneObjects[i].isVisible() || !sceneObjects[i].mesh)
        {
            continue;
        }
        // Adjust ray origin instead of translating all 3 triangle verts
        ray.origin = origin - sceneObjects[i].getPosition();
        for (int j = 0; j < sceneObjects[i].mesh->nFaces; j++)
        {
            tri = sceneObjects[i].mesh->faces[j].tri;
            faceWasHit = intersect(ray, tri, intersectPoint, uv);
            if (faceWasHit && intersectPoint.norm() < depth)
            {
                finalIntersectPoint = intersectPoint;
                depth = (intersectPoint - ray.origin).norm();
                objIndex = i;
                faceIndex = j;
                hitUV = uv;
            }
        }
    }
    if (objIndex >= 0)
    {
        rayIntersection.intersectPoint = finalIntersectPoint;
        rayIntersection.hitObject = &sceneObjects[objIndex];
        rayIntersection.hitMesh = sceneObjects[objIndex].mesh;
        rayIntersection.hitFace = &rayIntersection.hitMesh->faces[faceIndex];
        rayIntersection.uv = hitUV;
        rayIntersection.depth = depth;
    }
    return rayIntersection;
}
// TODO: cull tree nodes that are out of range (fundamentally unreachable)
RayIntersection ObjectManager::probeTree(Ray ray)
{
    RayIntersection rayIntersection;
    Vec3D_t intersectPoint, finalIntersectPoint;
    float depth = ray.tMax;
    bool intersected = false, faceHit = false;
    Vec2D_t uv, hitUV;

    const Face *hitFace = nullptr;
    Triangle currentTri;
    // Translate ray origin instead of all 3 face vertices
    ray.origin = ray.origin - sceneObjects[TERRAIN_INDEX].getPosition();

    terrainOctree.findHighestIntersectingVolume(ray);  // OCTREE
    //terrainOctree.search(ray);  // MINMAX
    auto volItr = terrainOctree.begin();    //TODO why doesn't this work with normal typing
    std::vector<Face*>::const_iterator faceItr;

    for (; volItr != terrainOctree.end(); volItr++)
    {
        for (faceItr = (*volItr)->begin(); faceItr != (*volItr)->end(); faceItr++)
        {
            currentTri = (*faceItr)->tri;
            intersected = intersect(ray, currentTri, intersectPoint, uv);
            if (intersected && intersectPoint.norm() < depth)  // see "highdea", Octree.h line 81
            {
                finalIntersectPoint = intersectPoint;
                depth = (intersectPoint - ray.origin).norm();
                hitFace = *faceItr;
                hitUV = uv;
                faceHit = true;
            }
        }
    }

    if (faceHit)
    {
        rayIntersection.intersectPoint = finalIntersectPoint;
        rayIntersection.hitObject = &sceneObjects[TERRAIN_INDEX];
        rayIntersection.hitMesh = sceneObjects[TERRAIN_INDEX].mesh;
        rayIntersection.hitFace = hitFace;
        rayIntersection.uv = hitUV;
        rayIntersection.depth = depth;
    }
    return rayIntersection;
}

int ObjectManager::getTotalSceneTris(bool visibleOnly)
{
    int total = 0;
    for (int i = 0; i < nObjects; i++)
    {
        if (sceneObjects[i].isVisible())
        {
            total += sceneObjects[i].mesh->nFaces;
        } else if (!visibleOnly)
        {
            total += sceneObjects[i].mesh->nFaces;
        }
    }
    return total;
}