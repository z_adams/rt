#include "ObjParser.h"
#include <sstream>

// Runs over all lines of the file and count number of each type of element
void ObjParser::count(std::ifstream & fs)
{
    std::string line;
    int verts = 0, tris = 0, tCoords = 0, norms = 0;
    while (std::getline(fs, line))
    {
        if (line[0] == 'v')
        {
            switch (line[1])
            {
                case ' ':
                    verts++;
                    break;
                case 't':
                    tCoords++;
                    break;
                case 'n':
                    norms++;
                    break;
                default:
                    break;
            }
        }
        if (line[0] == 'f')
        {
            tris++;
        }
    }
    fs.clear();
    fs.seekg(0, std::ios::beg);
    numVerts = verts;
    numFaces = tris;
    numNormals = norms;
    numUVs = tCoords;
}

void ObjParser::parse(std::ifstream & fs)
{
    std::string line;
    std::stringstream ss;
    int i, j = 0, k, vertIdx = 0, uvIdx = 0, normIdx = 0;
    Vec3D_t tmpVector;
    int tmpNorm, tmpUV;

    // parse vector lines (verts, norms, texCoords)
    while (std::getline(fs, line))
    {
        tmpVector.set(0.0f, 0.0f, 0.0f);
        if (line[0] == 'v' && line[1] == ' ')
        {
            i = 1;
        } else if (line[0] == 'v' && line[1] != ' ')
        {
            i = 2;
        } else
        {
            continue;
        }
        while (i++ < line.length() && line[i] != ' ')
        {
            ss.put(line[i]);
        }
        ss >> tmpVector.x;
        std::stringstream().swap(ss);
        while (i++ < line.length() && line[i] != ' ')
        {
            ss.put(line[i]);
        }
        ss >> tmpVector.y;
        std::stringstream().swap(ss);
        while (i++ < line.length() && line[i] != '\0')
        {
            ss.put(line[i]);
        }
        ss >> tmpVector.z;
        std::stringstream().swap(ss);
        switch (line[1])
        {
            case ' ':
                vertices[vertIdx] = tmpVector;
                vertIdx++;
                break;
            case 't':
                uvs[uvIdx] = static_cast<Vec2D_t>(tmpVector);
                uvIdx++;
                break;
            case 'n':
                vertNormals[normIdx] = tmpVector;
                normIdx++;
                break;
            default:
                break;
        }
    }
    fs.clear();
    fs.seekg(0, std::ios::beg);

    // Parse faces
    j = 0;
    while (std::getline(fs, line))
    {
        if (line[0] == 'f')
        {
            i = 1;
            // for each of the 3 vertices
            for (int l = 0; l < 3; l++)
            {
                while (i++ < line.length() && line[i] != '/' && line[i] != ' ')
                {
                    ss.put(line[i]);
                }
                ss >> k;
                tmpVector = vertices[k - 1];
                std::stringstream().swap(ss);
                while (i++ < line.length() && line[i] != '/' && line[i] != ' ')
                {
                    ss.put(line[i]);
                }
                ss >> tmpUV;
                std::stringstream().swap(ss);
                while (i++ < line.length() && line[i] != '/' && line[i] != ' ')
                {
                    ss.put(line[i]);
                }
                ss >> tmpNorm;
                std::stringstream().swap(ss);
                faces[j].addVert(l, tmpVector, tmpUV - 1, tmpNorm - 1);
            }
            j++;
        }
    }
    fs.clear();
    fs.seekg(0, std::ios::beg);
}

void ObjParser::processMesh(std::string meshFilepath, std::string textureFilepath)
{
    std::ifstream objFile(meshFilepath, std::ios::in);
    count(objFile);

    faces = std::shared_ptr<Face[]>(new Face[numFaces]);
    vertices = std::shared_ptr<Vec3D_t[]>(new Vec3D_t[numVerts]);
    vertNormals = std::shared_ptr<Vec3D_t[]>(new Vec3D_t[numNormals]);
    uvs = std::shared_ptr<Vec2D_t[]>(new Vec2D_t[numUVs]);

    currentMesh = std::shared_ptr<Mesh>(new Mesh(faces, vertices, vertNormals, uvs, numVerts, 
        numFaces, numNormals, numUVs, textureFilepath));
    parse(objFile);
    objFile.close();

    meshes.push_back(currentMesh);
    numMeshes++;
}

void ObjParser::createObject(int meshID, Vec3D_t position, Rotation rotation)
{
    SceneObject object(position, rotation, &(*meshes[meshID]));
    objects.push_back(object);
    numObjects++;
}