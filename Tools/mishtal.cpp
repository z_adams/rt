/*
Refactor:
* Disk I/O is a massive bottleneck, don't bother multithreading

* Consider using a work-stealing queue for threads https://blog.molecular-matters.com/2015/08/24/job-system-2-0-lock-free-work-stealing-part-1-basics/

* Parellelize based on ray, not quadrant (so that no threads will end up idle at the end).
  Allows for potential use of std::parallel:: namespace

* Use atomic index to keep track of ray indices: */
  std::Atomic<size_t> nextIndex{0}; // some global somewhere
  for(const auto & t : threads)
  {
    t.startRay(nextIndex++); // increment nextIndex after passing current value to function
  }
  // assume that startRay automatically starts the next ray by checking the value of "nextIndex"

// * Use a compare-exchange loop to avoid thread collisions for atomic writes
// * More than one number can be written atomically as long as it fits within 16B

  auto old_value = atomic_read(value);
  auto new_value = old_value combined with new data;
  while(true)
  {
      const auto current_value compare_exchange(value, old_value, new_value);
      if(current_value != old_value)
      {
          old_value = current_value;
          new_value = old_value combined with new data; // yes, do it again.
      }
      else
      {
           break;
      }
  }
 /*
 1. Read old value atomically
 2. Calculate desired new value
 3. Do atomic compare-and-swap
 	3.1 Compares value currently at memory addr
 	3.2 iff value == old value, replace with new_value
 	3.3 return whatever was previously at addr
 4. If returns old val, swap succeeded, and failed otherwise
 5. If swap failed, recalculate and try again
 - guaranteed to only repeat once per thing trying to write at a time

* Separate data and implementation parts of the program, allocate heavy data with std::shared
  Implementation code could either be a static or normal member, or just global

* Consider use of https://en.cppreference.com/w/cpp/thread/async for magic thread handling
* 2D index raycast sweeping with ints and decode index into elevation & azimuth
  - Atomically get next index
  - break index into azm & elevation with modulo
  - convert to float
* Read up on dependency injection, inversion of control